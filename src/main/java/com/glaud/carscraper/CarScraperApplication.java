package com.glaud.carscraper;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;

import com.glaud.carscraper.config.AppConfig;
import com.glaud.carscraper.config.MongoConfig;
import com.glaud.carscraper.config.RabbitConfig;

@SpringBootApplication
@Import({ AppConfig.class, MongoConfig.class, RabbitConfig.class })
@EnableEurekaClient
public class CarScraperApplication {

	public static void main(String[] args) {
		SpringApplication.run(CarScraperApplication.class, args);
	}

	@Bean
	CommandLineRunner init(DataInitializer dataInitializer) {
		return (args) -> {
			dataInitializer.initializeMarkNames();
		};
	}
}
