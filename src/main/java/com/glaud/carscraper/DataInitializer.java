package com.glaud.carscraper;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.glaud.carscraper.mongo.document.MarkName;
import com.glaud.carscraper.mongo.repository.MarkNameRepository;

@Component
public class DataInitializer {

	@Autowired
	private MarkNameRepository markNameRepository;

	public void initializeMarkNames() {
		if (markNameRepository.findAll().isEmpty()) {
			List<MarkName> markNames = createMarkNames();
			markNameRepository.saveAll(markNames);
		}
	}

	private List<MarkName> createMarkNames() {
		List<MarkName> markNames = new ArrayList<>();
		markNames.add(new MarkName("acura", "Acura"));
		markNames.add(new MarkName("aixam", "Aixam"));
		markNames.add(new MarkName("alfa-romeo", "Alfa Romeo"));
		markNames.add(new MarkName("aston-martin", "Aston Martin"));
		markNames.add(new MarkName("audi", "Audi"));
		markNames.add(new MarkName("bentley", "Bentley"));
		markNames.add(new MarkName("bmw", "BMW"));
		markNames.add(new MarkName("bugatti", "Bugatti"));
		markNames.add(new MarkName("buick", "Buick"));
		markNames.add(new MarkName("cadillac", "Cadillac"));
		markNames.add(new MarkName("chatenet", "Chatenet"));
		markNames.add(new MarkName("chevrolet", "Chevrolet"));
		markNames.add(new MarkName("chrysler", "Chrysler"));
		markNames.add(new MarkName("citroen", "Citroën"));
		markNames.add(new MarkName("dacia", "Dacia"));
		markNames.add(new MarkName("daewoo", "Daewoo"));
		markNames.add(new MarkName("daihatsu", "Daihatsu"));
		markNames.add(new MarkName("dodge", "Dodge"));
		markNames.add(new MarkName("ferrari", "Ferrari"));
		markNames.add(new MarkName("fiat", "Fiat"));
		markNames.add(new MarkName("ford", "Ford"));
		markNames.add(new MarkName("gaz", "Gaz"));
		markNames.add(new MarkName("honda", "Honda"));
		markNames.add(new MarkName("hummer", "Hummer"));
		markNames.add(new MarkName("hyundai", "Hyundai"));
		markNames.add(new MarkName("infiniti", "Infiniti"));
		markNames.add(new MarkName("isuzu", "Isuzu"));
		markNames.add(new MarkName("iveco", "Iveco"));
		markNames.add(new MarkName("jaguar", "Jaguar"));
		markNames.add(new MarkName("jeep", "Jeep"));
		markNames.add(new MarkName("kia", "Kia"));
		markNames.add(new MarkName("lada", "Lada"));
		markNames.add(new MarkName("lamborghini", "Lamborghini"));
		markNames.add(new MarkName("lancia", "Lancia"));
		markNames.add(new MarkName("land-rover", "Land Rover"));
		markNames.add(new MarkName("lexus", "Lexus"));
		markNames.add(new MarkName("ligier", "Ligier"));
		markNames.add(new MarkName("lincoln", "Lincoln"));
		markNames.add(new MarkName("lotus", "Lotus"));
		markNames.add(new MarkName("maserati", "Maserati"));
		markNames.add(new MarkName("maybach", "Maybach"));
		markNames.add(new MarkName("mazda", "Mazda"));
		markNames.add(new MarkName("mclaren", "McLaren"));
		markNames.add(new MarkName("mercedes", "Mercedes-Benz"));
		markNames.add(new MarkName("mercury", "Mercury"));
		markNames.add(new MarkName("mg", "MG"));
		markNames.add(new MarkName("microcar", "Microcar"));
		markNames.add(new MarkName("mini", "Mini"));
		markNames.add(new MarkName("mitsubishi", "Mitsubishi"));
		markNames.add(new MarkName("nissan", "Nissan"));
		markNames.add(new MarkName("opel", "Opel"));
		markNames.add(new MarkName("peugeot", "Peugeot"));
		markNames.add(new MarkName("plymouth", "Plymouth"));
		markNames.add(new MarkName("polonez", "Polonez"));
		markNames.add(new MarkName("pontiac", "Pontiac"));
		markNames.add(new MarkName("porsche", "Porsche"));
		markNames.add(new MarkName("renault", "Renault"));
		markNames.add(new MarkName("rolls-royce", "Rolls-Royce"));
		markNames.add(new MarkName("rover", "Rover"));
		markNames.add(new MarkName("saab", "Saab"));
		markNames.add(new MarkName("scion", "Scion"));
		markNames.add(new MarkName("seat", "Seat"));
		markNames.add(new MarkName("skoda", "Škoda"));
		markNames.add(new MarkName("smart", "Smart"));
		markNames.add(new MarkName("ssangyong", "SsangYong"));
		markNames.add(new MarkName("subaru", "Subaru"));
		markNames.add(new MarkName("suzuki", "Suzuki"));
		markNames.add(new MarkName("syrena", "Syrena"));
		markNames.add(new MarkName("tata", "Tata"));
		markNames.add(new MarkName("tesla", "Tesla"));
		markNames.add(new MarkName("toyota", "Toyota"));
		markNames.add(new MarkName("trabant", "Trabant"));
		markNames.add(new MarkName("uaz", "Uaz"));
		markNames.add(new MarkName("volkswagen", "Volkswagen"));
		markNames.add(new MarkName("volvo", "Volvo"));
		markNames.add(new MarkName("warszawa", "Warszawa"));
		markNames.add(new MarkName("wartburg", "Wartburg"));
		markNames.add(new MarkName("wolga", "Wołga"));
		markNames.add(new MarkName("yugo", "Yugo"));
		markNames.add(new MarkName("abarth", "Abarth"));
		return markNames;
	}

}
