package com.glaud.carscraper.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.glaud.carscraper.model.FuelQuantity;
import com.glaud.carscraper.model.RequestedCar;
import com.glaud.carscraper.service.FuelService;

@Controller
public class FuelController {

	private FuelService fuelService;

	@Autowired
	public FuelController(FuelService fuelService) {
		this.fuelService = fuelService;
	}

	@PostMapping("/findFuelConsumptionForObject")
	@ResponseBody
	public FuelQuantity findFuelQuantity(@RequestBody RequestedCar requestedCar) {
		System.out.println("requestedCar: " + requestedCar);
		return fuelService.findFuelConsumtion(requestedCar);
	}
	
	@GetMapping("/findFuelConsumption")
	@ResponseBody
	public FuelQuantity findFuelQuantity(@RequestParam("query") String query) {
		return fuelService.findFuelConsumtion(query);
	}


}
