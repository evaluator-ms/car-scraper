package com.glaud.carscraper.controller;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.glaud.carscraper.model.DetailsNames;
import com.glaud.carscraper.mongo.document.MarkName;
import com.glaud.carscraper.service.NamesService;

@Controller
public class NamesController {

	private NamesService namesService;

	public NamesController(NamesService namesService) {
		this.namesService = namesService;
	}

	@GetMapping("/findMarksNames")
	@ResponseBody
	public List<MarkName> findMarksNames() {
		return namesService.findMarkNames();
	}

	@GetMapping("/findModelsNames")
	@ResponseBody
	public Map<String, String> findModelsNames(@RequestParam("mark") String markName) {
		return namesService.findModelsNames(markName);
	}

	@GetMapping("/findDetailsNames")
	@ResponseBody
	public DetailsNames findDetailsNames(@RequestParam("query") String query) {
		return namesService.findDetails(query);
	}

}
