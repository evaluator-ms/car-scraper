package com.glaud.carscraper.controller;

import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.glaud.carscraper.mongo.document.Mark;
import com.glaud.carscraper.model.MarkAndModel;
import com.glaud.carscraper.mongo.document.Detail;
import com.glaud.carscraper.mongo.document.Model;
import com.glaud.carscraper.service.CarService;

@Controller
public class CarController {

	private CarService carService;

	public CarController(CarService carService) {
		this.carService = carService;
	}

	@GetMapping("/findMark")
	@ResponseBody
	public Mark findMark(@RequestParam("mark") String mark) {
		return carService.findInfoAboutMark(mark);
	}

	@GetMapping("/getModels")
	@ResponseBody
	public List<Model> getModelsNames(@RequestParam("mark") String mark) {
		System.out.println("in getModels");
		return carService.findInfoAboutMark(mark).getModels();
	}

	@PostMapping("/getGenerations")
	@ResponseBody
	public List<Detail> getGenerations(@RequestParam("mark") String mark, @RequestParam("model") String modelName) {
		System.out.println("in getGenerations");
		return carService.findInfoAboutModel(new MarkAndModel(mark, modelName)).getDetails();
	}

	@GetMapping("/getModel")
	@ResponseBody
	public Model getModel(@RequestParam("mark") String mark, @RequestParam("model") String modelName) {
		return carService.findInfoAboutModel(new MarkAndModel(mark, modelName));
	}
}
