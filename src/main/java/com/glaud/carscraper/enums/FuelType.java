package com.glaud.carscraper.enums;

public enum FuelType {
	GASOLINE, DIESEL, LPG, UNKNOWN

}
