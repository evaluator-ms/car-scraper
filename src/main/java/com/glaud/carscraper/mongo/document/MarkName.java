package com.glaud.carscraper.mongo.document;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class MarkName {

	private String name;
	private String value;

	public MarkName(String value, String name) {
		super();
		this.value = value;
		this.name = name;
	}
}
