package com.glaud.carscraper.mongo.document;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class Detail {

	public static Detail NOT_FOUND = new Detail.Builder("Details not found").build();

	private String name;
	private String prodYears;
	private String rate;

	public static class Builder {
		private String name;
		private String prodYears;
		private String rate;

		public Builder(String name) {
			this.name = name;
		}

		public Builder produced(String prodYears) {
			this.prodYears = prodYears;
			return this;
		}

		public Builder withRate(String rate) {
			this.rate = rate;
			return this;
		}

		public Detail build() {
			Detail detail = new Detail();
			detail.name = this.name;
			detail.prodYears = this.prodYears;
			detail.rate = this.rate;
			return detail;
		}
	}

	private Detail() {
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((prodYears == null) ? 0 : prodYears.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Detail other = (Detail) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (prodYears == null) {
			if (other.prodYears != null)
				return false;
		} else if (!prodYears.equals(other.prodYears))
			return false;
		return true;
	}

}
