package com.glaud.carscraper.mongo.document;

import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Document
@Getter
@Setter
@ToString
public class Mark {

	public static Mark NOT_FOUND = new Mark.Builder("Mark not found").build();

	@Id
	private String id;
	@Indexed(unique = true)
	private String name;
	private String rate;
	private String originCountry;
	private String priceRange;
	private List<Model> models;

	public static class Builder {
		private String name;
		private String rate;
		private String originCountry;
		private String priceRange;
		private List<Model> models;

		public Builder(String name) {
			this.name = name;
		}

		public Builder withModels(List<Model> models) {
			this.models = models;
			return this;
		}

		@Override
		public String toString() {
			return "Builder [name=" + name + ", originCountry=" + originCountry + ", priceRange=" + priceRange
					+ ", models=" + models + "]";
		}

		public Builder withRate(String rate) {
			this.rate = rate;
			return this;
		}

		public Builder from(String originCountry) {
			this.originCountry = originCountry;
			return this;
		}

		public Builder withPriceRange(String priceRange) {
			this.priceRange = priceRange.substring(0, priceRange.length() - 4);
			return this;
		}

		public Mark build() {
			Mark car = new Mark();
			car.name = this.name;
			car.models = this.models;
			car.rate = this.rate;
			car.originCountry = this.originCountry;
			car.priceRange = this.priceRange;
			return car;
		}
	}

	private Mark() {
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((models == null) ? 0 : models.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((originCountry == null) ? 0 : originCountry.hashCode());
		result = prime * result + ((priceRange == null) ? 0 : priceRange.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Mark other = (Mark) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (models == null) {
			if (other.models != null)
				return false;
		} else if (!models.equals(other.models))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (originCountry == null) {
			if (other.originCountry != null)
				return false;
		} else if (!originCountry.equals(other.originCountry))
			return false;
		if (priceRange == null) {
			if (other.priceRange != null)
				return false;
		} else if (!priceRange.equals(other.priceRange))
			return false;
		return true;
	}

}
