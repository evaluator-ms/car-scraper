package com.glaud.carscraper.mongo.document;

import java.util.List;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class Model {

	public static Model NOT_FOUND = new Model.Builder("Model not found").build();

	private String name;
	private String prodYears;
	private String rate;
	private String priceStartingAt;
	private List<Detail> details;

	public static class Builder {
		private String name;
		private String prodYears;
		private String rate;
		private String priceStartingAt;
		private List<Detail> details;

		public Builder(String name) {
			this.name = name;
		}

		public Builder withDetails(List<Detail> details) {
			this.details = details;
			return this;
		}

		public Builder produced(String prodYears) {
			this.prodYears = prodYears;
			return this;
		}

		public Builder withRate(String rate) {
			this.rate = rate;
			return this;
		}

		public Builder withPriceStartingAt(String price) {
			this.priceStartingAt = price;
			return this;
		}

		public Model build() {
			Model model = new Model();
			model.name = this.name;
			model.prodYears = this.prodYears;
			model.rate = this.rate;
			model.details = this.details;
			model.priceStartingAt = this.priceStartingAt;
			return model;
		}
	}

	private Model() {
	}
	
	

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((details == null) ? 0 : details.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((priceStartingAt == null) ? 0 : priceStartingAt.hashCode());
		result = prime * result + ((prodYears == null) ? 0 : prodYears.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Model other = (Model) obj;
		if (details == null) {
			if (other.details != null)
				return false;
		} else if (!details.equals(other.details))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (priceStartingAt == null) {
			if (other.priceStartingAt != null)
				return false;
		} else if (!priceStartingAt.equals(other.priceStartingAt))
			return false;
		if (prodYears == null) {
			if (other.prodYears != null)
				return false;
		} else if (!prodYears.equals(other.prodYears))
			return false;
		return true;
	}
}
