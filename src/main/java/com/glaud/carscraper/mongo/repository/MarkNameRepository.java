package com.glaud.carscraper.mongo.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.glaud.carscraper.mongo.document.MarkName;

@Repository
public interface MarkNameRepository extends MongoRepository<MarkName, String> {

}
