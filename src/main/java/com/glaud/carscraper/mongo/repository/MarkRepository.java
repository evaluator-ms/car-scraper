package com.glaud.carscraper.mongo.repository;

import java.util.Optional;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.glaud.carscraper.mongo.document.Mark;

@Repository
public interface MarkRepository extends MongoRepository<Mark, String> {
	
	Optional<Mark> findByName(String name);

}
