package com.glaud.carscraper.config;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.Exchange;
import org.springframework.amqp.core.ExchangeBuilder;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.QueueBuilder;
import org.springframework.amqp.rabbit.annotation.RabbitListenerConfigurer;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.rabbit.listener.RabbitListenerEndpointRegistrar;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.converter.MappingJackson2MessageConverter;
import org.springframework.messaging.handler.annotation.support.DefaultMessageHandlerMethodFactory;
import org.springframework.messaging.handler.annotation.support.MessageHandlerMethodFactory;
import org.springframework.retry.backoff.ExponentialBackOffPolicy;
import org.springframework.retry.support.RetryTemplate;

@Configuration
public class RabbitConfig implements RabbitListenerConfigurer {
	public static final String QUEUE_MODELS = "models-queue";
	public static final String QUEUE_MODELS_INFO = "models-info-queue";
	public static final String EXCHANGE_MODELS = "models-exchange";
	public static final String QUEUE_FUEL = "fuel-queue";
	public static final String EXCHANGE_FUEL = "fuel-exchange";

	@Bean
	Queue modelsQueue() {
		return QueueBuilder.durable(QUEUE_MODELS).build();
	}

	@Bean
	Queue modelsInfoQueue() {
		return QueueBuilder.durable(QUEUE_MODELS_INFO).build();
	}

	@Bean
	Queue fuelQueue() {
		return QueueBuilder.durable(QUEUE_FUEL).build();
	}

	@Bean
	Exchange modelsExchange() {
		return ExchangeBuilder.directExchange(EXCHANGE_MODELS).build();
	}

	@Bean
	Exchange fuelExchange() {
		return ExchangeBuilder.directExchange(EXCHANGE_FUEL).build();
	}

	@Bean
	Binding modelsBinding(DirectExchange modelsExchange) {
		return BindingBuilder.bind(modelsQueue()).to(modelsExchange).with("rpc.models");
	}
	
	@Bean
	Binding modelsInfoBinding(DirectExchange modelsExchange) {
		return BindingBuilder.bind(modelsInfoQueue()).to(modelsExchange).with("rpc.models.info");
	}

	@Bean
	Binding fuelBinding(DirectExchange fuelExchange) {
		return BindingBuilder.bind(fuelQueue()).to(fuelExchange).with("rpc.fuel");
	}

	@Bean
	public RetryTemplate retryTemplate() {
		RetryTemplate retryTemplate = new RetryTemplate();
		ExponentialBackOffPolicy backOffPolicy = new ExponentialBackOffPolicy();
		backOffPolicy.setInitialInterval(500);
		backOffPolicy.setMultiplier(10.0);
		backOffPolicy.setMaxInterval(10000);
		retryTemplate.setBackOffPolicy(backOffPolicy);
		return retryTemplate;
	}

	@Bean
	public RabbitTemplate rabbitTemplate(final ConnectionFactory connectionFactory) {
		final RabbitTemplate rabbitTemplate = new RabbitTemplate(connectionFactory);
		rabbitTemplate.setMessageConverter(producerJackson2MessageConverter());
		rabbitTemplate.setRetryTemplate(retryTemplate());
		return rabbitTemplate;
	}

	@Bean
	public Jackson2JsonMessageConverter producerJackson2MessageConverter() {
		return new Jackson2JsonMessageConverter();
	}

	@Override
	public void configureRabbitListeners(RabbitListenerEndpointRegistrar registrar) {
		registrar.setMessageHandlerMethodFactory(messageHandlerMethodFactory());
	}

	@Bean
	MessageHandlerMethodFactory messageHandlerMethodFactory() {
		DefaultMessageHandlerMethodFactory messageHandlerMethodFactory = new DefaultMessageHandlerMethodFactory();
		messageHandlerMethodFactory.setMessageConverter(consumerJackson2MessageConverter());
		return messageHandlerMethodFactory;
	}

	@Bean
	public MappingJackson2MessageConverter consumerJackson2MessageConverter() {
		return new MappingJackson2MessageConverter();
	}
}