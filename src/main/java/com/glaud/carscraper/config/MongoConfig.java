package com.glaud.carscraper.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.config.AbstractMongoConfiguration;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

import com.mongodb.MongoClient;

@Configuration
@EnableMongoRepositories(basePackages = "com.glaud.carscraper.mongo.repository")
public class MongoConfig extends AbstractMongoConfiguration {

	@Value("${mongo.host:127.0.0.1}")
	private String mongoHost;

	@Value("${mongo.port:27017}")
	private int mongoPort;
	
	@Override
	protected String getDatabaseName() {
		return "cars";
	}

	@Override
	public MongoClient mongoClient() {
		return new MongoClient(mongoHost, mongoPort);
	}

	@Override
	protected String getMappingBasePackage() {
		return "com.glaud.carscraper.mongo";
	}
}