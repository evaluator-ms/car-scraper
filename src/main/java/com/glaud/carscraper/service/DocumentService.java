package com.glaud.carscraper.service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import org.springframework.stereotype.Service;

@Service
public class DocumentService {

	public Document getDocument(String url, String query) throws IOException {
		query = query.toLowerCase().replace(" ", "-").replaceAll("[()]", "");
		query = query.toLowerCase().replace(".", "");
		query = StringUtils.stripAccents(query.toLowerCase());
		Connection connection = Jsoup.connect(url + query).timeout(15000).userAgent(
				"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/33.0.1750.152 Safari/537.36");
		Document doc;
		doc = connection.get();
		return doc;
	}

	List<String> getNamesOnPage(Document page) {
		Elements elements = page.getElementsByClass("name-of-the-car");
		List<String> names = new ArrayList<>();
		elements.forEach(element -> {
			names.add(element.text());
		});
		return names;
	}

}
