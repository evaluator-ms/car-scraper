package com.glaud.carscraper.service;

import com.glaud.carscraper.model.FuelQuantity;
import com.glaud.carscraper.model.RequestedCar;

public interface FuelService {

	FuelQuantity findFuelConsumtion(String carQuery);

	FuelQuantity findFuelConsumtion(RequestedCar requestedCar);
}
