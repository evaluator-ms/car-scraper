package com.glaud.carscraper.service;

import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class WebScraperHelper {

	public static String prepareQuery(String query) {
		return query.startsWith("/") ? query.substring(1) : query;
	}

	public static Double parseToDouble(Elements elements, int index) {
		Element element;
		if ((element = elements.get(index)) != null) {
			return Double.parseDouble(element.text().replace(",", "."));
		}
		return null;
	}

}
