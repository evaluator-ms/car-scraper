package com.glaud.carscraper.service;

import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.glaud.carscraper.model.DetailsNames;
import com.glaud.carscraper.mongo.document.MarkName;
import com.glaud.carscraper.mongo.repository.MarkNameRepository;

@Service
public class NamesService {

	@Value("${autocentrum.url.main}")
	private String url;

	private DocumentService documentService;

	private MarkNameRepository markNameRepository;

	@Autowired
	public NamesService(DocumentService documentService, MarkNameRepository markNameRepository) {
		this.documentService = documentService;
		this.markNameRepository = markNameRepository;
	}

	public List<MarkName> findMarkNames() {
		return markNameRepository.findAll();
	}

	public Map<String, String> findModelsNames(String markName) {
		Document markMainPage = null;
		try {
			markMainPage = documentService.getDocument(url, markName);
		} catch (IOException e) {
			e.printStackTrace();
			return Collections.emptyMap();
		}
		return getNamesWithLink(markMainPage);
	}

	public DetailsNames findDetails(String query) {
		Document page = null;
		try {
			page = documentService.getDocument(url, WebScraperHelper.prepareQuery(query));
		} catch (IOException e) {
			e.printStackTrace();
		}
		Map<String, String> engines = findEnginesNames(page);
		if (!engines.isEmpty()) {
			return new DetailsNames(engines, true);
		}
		Map<String, String> detailsNames = getNamesWithLink(page);
		return new DetailsNames(detailsNames, false);
	}

	Map<String, String> getNamesWithLink(Document page) {
		Element selectorDiv = page.getElementsByClass("car-selector-box-row").first();
		Elements aElements = selectorDiv.getElementsByTag("a");
		Map<String, String> names = new TreeMap<>();
		aElements.forEach(a -> {
			String name = a.getElementsByClass("name-of-the-car").first().ownText();
			names.put(name, a.attr("href"));
		});
		return names;
	}

	private Map<String, String> findEnginesNames(Document modelMainPage) {
		Map<String, String> engines = new TreeMap<>();
		modelMainPage.select("a.engine-link").stream().forEach(a -> {
			String href = a.attr("href");
			engines.put(a.ownText(), href);
		});
		return engines;
	}
}
