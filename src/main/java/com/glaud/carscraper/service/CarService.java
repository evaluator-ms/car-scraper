package com.glaud.carscraper.service;

import java.util.List;

import org.jsoup.nodes.Document;

import com.glaud.carscraper.mongo.document.Mark;
import com.glaud.carscraper.model.MarkAndModel;
import com.glaud.carscraper.mongo.document.Detail;
import com.glaud.carscraper.mongo.document.Model;

public interface CarService {

	Mark findInfoAboutMark(String mark);

	Model findInfoAboutModel(MarkAndModel markAndModel);

	List<Detail> findModelDetails(Document modelMainPage);

}
