package com.glaud.carscraper.service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.glaud.carscraper.mongo.document.Mark;
import com.glaud.carscraper.config.RabbitConfig;
import com.glaud.carscraper.model.MarkAndModel;
import com.glaud.carscraper.mongo.document.Detail;
import com.glaud.carscraper.mongo.document.Model;
import com.glaud.carscraper.mongo.repository.MarkRepository;

@Service
public class CarServiceImpl implements CarService {

	@Value("${autocentrum.url.main}")
	private String url;

	private DocumentService documentService;
	private MarkRepository markRepository;

	@Autowired
	public CarServiceImpl(DocumentService documentService, MarkRepository markRepository) {
		this.documentService = documentService;
		this.markRepository = markRepository;
	}

	@Override
	@RabbitListener(queues = RabbitConfig.QUEUE_MODELS)
	public Mark findInfoAboutMark(String markName) {
		System.out.println("Received request. Finding info about: " + markName);
		Optional<Mark> optMark = checkIfMarkAlreadyInDb(markName);
		if (optMark.isPresent()) {
			return optMark.get();
		}
		Mark mark = getInfoAboutMark(markName);
		System.out.println("Sending respone: " + mark);
		if (!mark.equals(Mark.NOT_FOUND)) {
			markRepository.save(mark);
		}
		return mark;
	}

	private Optional<Mark> checkIfMarkAlreadyInDb(String mark) {
		Optional<Mark> optCar = markRepository.findByName(mark);
		return optCar;
	}

	private Mark getInfoAboutMark(String markName) {
		Document markMainPage = null;
		try {
			markMainPage = documentService.getDocument(url, markName);
		} catch (IOException e) {
			e.printStackTrace();
			return Mark.NOT_FOUND;
		}
		String rate = findRate(markMainPage);
		Element country = markMainPage.getElementsByClass("make-info__country__name").first();
		// TODO update
		String originCountry = country != null ? country.getElementsByTag("strong").text() : null;
		String priceRange = findPriceRange(markMainPage);
		List<Model> models = findModels(markName, markMainPage);
		Mark car = new Mark.Builder(markName).withModels(models).withRate(rate).from(originCountry)
				.withPriceRange(priceRange).build();
		return car;
	}

	private String findPriceRange(Document markMainPage) {
		Elements priceElements = markMainPage.getElementsByClass("price-header");
		String priceRange = "niedostępne";
		if (!priceElements.isEmpty()) {
			priceRange = priceElements.first().getElementsByTag("span").text();
		}
		return priceRange;
	}

	private List<Model> findModels(String markName, Document markMainPage) {
		List<String> modelsNames = documentService.getNamesOnPage(markMainPage);
		List<Model> models = new ArrayList<>();
		modelsNames.forEach(modelName -> {
			models.add(findInfoAboutModel(new MarkAndModel(markName, modelName)));
		});
		return models;
	}

	@Override
	@RabbitListener(queues = RabbitConfig.QUEUE_MODELS_INFO)
	public Model findInfoAboutModel(MarkAndModel markAndModel) {
		System.out.println(markAndModel);
		String markName = markAndModel.getMark();
		String modelName = markAndModel.getModel().toLowerCase().replaceAll("(/)|( & )", "-");
		Document modelMainPage = null;
		try {
			modelMainPage = documentService.getDocument(url, markName + "/" + modelName);
		} catch (IOException e) {
			e.printStackTrace();
			return Model.NOT_FOUND;
		}
		String prodYears = findProdYears(modelMainPage);
		String rate = findRate(modelMainPage);
		String priceStartingAt = findPriceStartingAt(modelMainPage);
		List<Detail> details = findModelDetails(modelMainPage);
		Model model = new Model.Builder(modelName).produced(prodYears).withDetails(details).withRate(rate)
				.withPriceStartingAt(priceStartingAt).build();
		return model;
	}

	private String findRate(Document page) {
		Elements rateElements = page.getElementsByClass("part__info__value");
		String rate = "brak ocen";
		if (!rateElements.isEmpty()) {
			rate = rateElements.first().text();
		}
		return rate;
	}

	private String findProdYears(Document modelMainPage) {
		String prodYears = "nieokreślono";
		Elements prodYearsElements = modelMainPage.getElementsByClass("prod-years");
		if (!prodYearsElements.isEmpty()) {
			prodYears = prodYearsElements.first().getElementsByTag("strong").first().text();
		}
		return prodYears;
	}

	private String findPriceStartingAt(Document page) {
		Elements priceInfo = page.getElementsByClass("price-info");
		String priceStartingAt = "niedostępny jako nowy";
		if (!priceInfo.isEmpty()) {
			priceStartingAt = priceInfo.first().getElementsByTag("strong").first().text().replace('\u00A0', ' ');
		}
		return priceStartingAt;
	}

	@Override
	public List<Detail> findModelDetails(Document modelMainPage) {
		List<Detail> details = new ArrayList<>();
		List<String> detailsNames = documentService.getNamesOnPage(modelMainPage);
		System.out.println("detailsNames: " + detailsNames);
		String modelUrl = modelMainPage.baseUri();
		detailsNames.forEach(detailName -> {
			details.add(findDetail(modelUrl, detailName));
		});
		return details;
	}

	private Detail findDetail(String modelUrl, String detailName) {
		String[] parts = detailName.split("\\(");
		String name = parts[0].trim().toLowerCase();
		String prodYears = parts[1].trim();
		String productionYears = prodYears.substring(0, prodYears.length() - 1);
		Document generationMainPage;
		try {
			generationMainPage = documentService.getDocument(modelUrl, name);
		} catch (IOException e) {
			e.printStackTrace();
			return Detail.NOT_FOUND;
		}
		return new Detail.Builder(name).produced(productionYears).withRate(findRate(generationMainPage)).build();
	}

}
