package com.glaud.carscraper.service;

import java.io.IOException;

import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.glaud.carscraper.config.RabbitConfig;
import com.glaud.carscraper.model.FuelQuantity;
import com.glaud.carscraper.model.FuelQuantity.Status;
import com.glaud.carscraper.model.RequestedCar;

@Service
public class FuelServiceImpl implements FuelService {

	@Value("${autocentrum.url.fuel}")
	private String fuelUrl;

	private DocumentService documentService;

	@Autowired
	public FuelServiceImpl(DocumentService documentService) {
		this.documentService = documentService;
	}

	@Override
	@RabbitListener(queues = RabbitConfig.QUEUE_FUEL)
	public FuelQuantity findFuelConsumtion(String carQuery) {
		System.out.println("Received and proccessing car: " + carQuery);
		Document markMainPage = null;
		try {
			markMainPage = documentService.getDocument(fuelUrl, WebScraperHelper.prepareQuery(carQuery));
		} catch (IOException e) {
			e.printStackTrace();
			return FuelQuantity.ERROR;
		}
		Elements elements = markMainPage.select("div.value");
		if (elements.isEmpty()) {
			return FuelQuantity.NOT_FOUND;
		}
		FuelQuantity fuelQuantity = buildFuelQuantity(markMainPage, elements);
		System.out.println("Sending back response: " + fuelQuantity);
		return fuelQuantity;
	}

	private FuelQuantity buildFuelQuantity(Document markMainPage, Elements elements) {
		Double avgConsumption = WebScraperHelper.parseToDouble(elements, 0);
		Double declaredConsumption = WebScraperHelper.parseToDouble(elements, 1);
		String fuelName = markMainPage.getElementsByClass("fuel-name").first().text();
		FuelQuantity fuelQuantity = new FuelQuantity(avgConsumption, declaredConsumption, fuelName, Status.OK);
		return fuelQuantity;
	}

	@Override
	public FuelQuantity findFuelConsumtion(RequestedCar requestedCar) {
		System.out.println("Received and proccessing car: " + requestedCar);
		return findFuelConsumtion(requestedCar.buildQuery());
	}

}
