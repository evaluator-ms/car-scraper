package com.glaud.carscraper.model;

import java.util.ArrayList;
import java.util.List;

import lombok.Data;

@Data
public class RequestedCar {

	private String mark;
	private String model;
	private String generation;
	private String version;
	private String engine;

	private RequestedCar() {
	}

	public static class Builder {
		private String mark;
		private String model;
		private String generation;
		private String version;
		private String engine;

		public Builder(String mark, String model, String engine) {
			this.mark = mark;
			this.model = model;
			this.engine = engine;
		}

		public Builder generation(String generation) {
			this.generation = generation;
			return this;
		}

		public Builder version(String version) {
			this.version = version;
			return this;
		}

		public RequestedCar build() {
			RequestedCar requestedCar = new RequestedCar();
			requestedCar.mark = mark;
			requestedCar.model = model;
			requestedCar.generation = generation;
			requestedCar.version = version;
			requestedCar.engine = engine;
			return requestedCar;
		}
	}

	public String buildQuery() {
		List<String> elements = new ArrayList<>();
		elements.add(this.getMark());
		elements.add(this.getModel());
		if (this.getGeneration() != null) {
			elements.add(this.getGeneration());
		}
		if (this.getVersion() != null) {
			elements.add(this.getVersion());
		}
		elements.add(this.getEngine());
		return String.join("/", elements);
	}

}
