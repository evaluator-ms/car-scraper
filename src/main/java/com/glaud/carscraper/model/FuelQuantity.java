package com.glaud.carscraper.model;

import com.glaud.carscraper.enums.FuelType;

import lombok.Data;

@Data
public class FuelQuantity {

	public static FuelQuantity ERROR = new FuelQuantity(Status.ERROR);
	public static FuelQuantity NOT_FOUND = new FuelQuantity(Status.NOT_FOUND);

	private Double avgConsumption;
	private Double declaredConsumption;
	private FuelType fuelType;
	private Status status;

	public enum Status {
		OK, NOT_FOUND, ERROR
	}

	public FuelQuantity(Status status) {
		this.status = status;
	}

	public FuelQuantity() {
	}

	public FuelQuantity(Double avgConsumption, Double declaredConsumption, FuelType fuelType, Status status) {
		this.avgConsumption = avgConsumption;
		this.declaredConsumption = declaredConsumption;
		this.fuelType = fuelType;
		this.status = status;
	}

	public FuelQuantity(Double avgConsumption, Double declaredConsumption, String fuelName, Status status) {
		this.avgConsumption = avgConsumption;
		this.declaredConsumption = declaredConsumption;
		this.fuelType = parseFuelName(fuelName);
		this.status = status;
	}

	private FuelType parseFuelName(String fuelName) {
		switch (fuelName) {
		case "pb":
			return FuelType.GASOLINE;
		case "on":
			return FuelType.DIESEL;
		case "lpg":
			return FuelType.LPG;
		default:
			return FuelType.UNKNOWN;
		}
	}

}
