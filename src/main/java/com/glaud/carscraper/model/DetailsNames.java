package com.glaud.carscraper.model;

import java.util.Map;

import lombok.Data;

@Data
public class DetailsNames {

	private Map<String, String> names;
	private boolean areEnginesReady;

	public DetailsNames(Map<String, String> names, boolean areEnginesReady) {
		this.names = names;
		this.areEnginesReady = areEnginesReady;
	}

}
