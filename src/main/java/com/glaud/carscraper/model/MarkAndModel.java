package com.glaud.carscraper.model;

import lombok.Data;

@Data
public class MarkAndModel {

	private String mark;
	private String model;

	public MarkAndModel() {
	}

	public MarkAndModel(String mark, String model) {
		super();
		this.mark = mark;
		this.model = model;
	}
}
