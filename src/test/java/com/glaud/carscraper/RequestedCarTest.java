package com.glaud.carscraper;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.glaud.carscraper.model.RequestedCar;

@ExtendWith(SpringExtension.class)
public class RequestedCarTest {

	private RequestedCar requestedCar;

	@Test
	public void buildQueryWithGenAndVersionTest() {
		requestedCar = new RequestedCar.Builder("audi", "a3", "silnik-benzynowy-1.6-102km-2003-2010").generation("8p")
				.version("hatchback-3d").build();
		assertEquals("audi/a3/8p/hatchback-3d/silnik-benzynowy-1.6-102km-2003-2010", requestedCar.buildQuery());
	}

	@Test
	public void buildQueryWithVersionTest() {
		requestedCar = new RequestedCar.Builder("alfa-romeo", "147", "silnik-diesla-1.9-jtd-100km-2005-2010")
				.version("hatchback").build();
		assertEquals("alfa-romeo/147/hatchback/silnik-diesla-1.9-jtd-100km-2005-2010", requestedCar.buildQuery());
	}

	@Test
	public void buildQueryOnlyEngineTest() {
		requestedCar = new RequestedCar.Builder("alfa-romeo", "stelvio", "silnik-benzynowy-2.0-turbo-200km-od-2017")
				.build();
		assertEquals("alfa-romeo/stelvio/silnik-benzynowy-2.0-turbo-200km-od-2017", requestedCar.buildQuery());
	}
}
