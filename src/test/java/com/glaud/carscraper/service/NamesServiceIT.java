package com.glaud.carscraper.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Map;
import java.util.TreeMap;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.util.ReflectionTestUtils;

import com.glaud.carscraper.model.DetailsNames;
import com.glaud.carscraper.mongo.repository.MarkNameRepository;

@ExtendWith(SpringExtension.class)
public class NamesServiceIT {

	private NamesService namesService;
	private Map<String, String> names;
	private String query;
	private DetailsNames expectedDetailsNames;

	@Mock
	private MarkNameRepository markNameRepository;

	@BeforeEach
	public void setUp() {
		namesService = new NamesService(new DocumentService(), markNameRepository);
		names = new TreeMap<>();
		ReflectionTestUtils.setField(namesService, "url", "https://www.autocentrum.pl/");
	}

	@Test
	public void findModelsNamesTest() {
		names = namesService.findModelsNames("audi");
		System.out.println(names);
		assertTrue(names.containsKey("A4") && names.containsValue("/audi/a4/"));
	}

	@Test
	public void findDetailsTest() {
		query = "porsche/cayenne/";
		names.put("I (2002 - 2010)", "/porsche/cayenne/i/");
		names.put("II (2010 - 2017)", "/porsche/cayenne/ii/");
		names.put("III (2017 - teraz)", "/porsche/cayenne/iii/");
		expectedDetailsNames = new DetailsNames(names, false);
		DetailsNames actualDetailsNames = namesService.findDetails(query);
		assertEquals(expectedDetailsNames, actualDetailsNames);
	}

	@Test
	public void findEnginesTest() {
		query = "/audi/r8/ii/coupe/";
		names.put("5.2 V10 540KM 397kW (2015-2018)", "/audi/r8/ii/coupe/silnik-benzynowy-5.2-v10-540km-2015-2018/");
		names.put("5.2 V10 Plus 610KM 449kW (2015-2018)",
				"/audi/r8/ii/coupe/silnik-benzynowy-5.2-v10-plus-610km-2015-2018/");
		expectedDetailsNames = new DetailsNames(names, true);
		DetailsNames actualDetailsNames = namesService.findDetails(query);
		assertEquals(expectedDetailsNames, actualDetailsNames);
	}

}
