package com.glaud.carscraper.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import static org.mockito.Mockito.*;

import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.util.ReflectionTestUtils;

import com.glaud.carscraper.model.MarkAndModel;
import com.glaud.carscraper.mongo.document.Detail;
import com.glaud.carscraper.mongo.document.Mark;
import com.glaud.carscraper.mongo.document.Model;
import com.glaud.carscraper.mongo.repository.MarkRepository;

@ExtendWith(SpringExtension.class)
public class CarServiceImplIT {

	private CarService carServiceImpl;

	@Mock
	private MarkRepository carRepository;

	private Mark expectedMark;
	private List<Model> expectedModels;
	private List<Detail> details;
	private List<Detail> audiDetails;

	@BeforeEach
	public void setUp() {
		carServiceImpl = new CarServiceImpl(new DocumentService(), carRepository);
		ReflectionTestUtils.setField(carServiceImpl, "url", "https://www.autocentrum.pl/");
		setUpMark();
		setUpDetailsForAudi();
	}

	private void setUpMark() {
		setUpModels();
		expectedMark = new Mark.Builder("daihatsu").withModels(expectedModels).withRate("4,14")//.from("Japonia")
				.withPriceRange("niedostępne").build();

	}

	private void setUpModels() {
		setUpDetails();
		expectedModels = new ArrayList<>();
		Model charade = new Model.Builder("charade").produced("1977 – 2000").withDetails(details).withRate("4,07")
				.withPriceStartingAt("niedostępny jako nowy").build();
		Model cuore = new Model.Builder("cuore").build();
		Model materia = new Model.Builder("materia").build();
		Model move = new Model.Builder("move").build();
		Model terios = new Model.Builder("terios").build();
		Model _850 = new Model.Builder("850").build();
		Model applause = new Model.Builder("applause").build();
		Model feroza = new Model.Builder("feroza").build();
		Model granMove = new Model.Builder("gran move").build();
		Model hijet = new Model.Builder("hijet").build();
		Model opti = new Model.Builder("opti").build();
		Model rocky = new Model.Builder("rocky").build();
		Model sirion = new Model.Builder("sirion").build();
		Model trevis = new Model.Builder("trevis").build();
		Model yrv = new Model.Builder("yrv").build();
		expectedModels.addAll(Arrays.asList(cuore, materia, move, terios, _850, applause, charade, feroza, granMove, hijet,
				opti, rocky, sirion, trevis, yrv));
	}

	private void setUpDetails() {
		details = new ArrayList<>();
		Detail g10 = new Detail.Builder("g10").produced("1977 - 1983").withRate("3,56").build();
		Detail g11 = new Detail.Builder("g11").produced("1983 - 1987").withRate("3,89").build();
		Detail g100 = new Detail.Builder("g100").produced("1987 - 1994").withRate("4,06").build();
		Detail g200 = new Detail.Builder("g200").produced("1994 - 2000").withRate("4,32").build();
		details.addAll(Arrays.asList(g200, g100, g11, g10));
	}

	private void setUpDetailsForAudi() {
		audiDetails = new ArrayList<>();
		Detail _8v = new Detail.Builder("8v").produced("2012 - teraz").withRate("4,63").build();
		Detail _8p = new Detail.Builder("8p").produced("2003 - 2013").withRate("4,32").build();
		Detail _8l = new Detail.Builder("8l").produced("1996 - 2003").withRate("4,26").build();
		audiDetails.addAll(Arrays.asList(_8v, _8p, _8l));
	}

	private List<String> getModelNames(List<Model> models) {
		return models.stream().map(model -> model.getName()).collect(Collectors.toList());
	}

	private Model findModelByName(List<Model> models, String modelName) {
		return models.stream().filter(model -> model.getName().equals(modelName)).findFirst().get();
	}

	@Test
	public void findInfoAboutMarkTest() {
		String markName = "daihatsu";
		String modelName = "charade";
		Mark mark = carServiceImpl.findInfoAboutMark(markName);
		when(carRepository.findByName(markName)).thenReturn(Optional.empty());
		List<Model> actualModels = mark.getModels();
		List<String> actualModelsNames = getModelNames(actualModels);
		assertEquals(getModelNames(expectedModels), actualModelsNames, "equals models names assertion");
		assertEquals(findModelByName(expectedModels, modelName), findModelByName(actualModels, modelName),
				"equals specific model assertion");
	}

	@Test
	public void findInfoAboutModelTest() {
		Model actualModel = carServiceImpl.findInfoAboutModel(new MarkAndModel("audi", "A3"));
		Model expectedModel = new Model.Builder("a3").produced("1996 – teraz").withDetails(audiDetails).withRate("4,28")
				.withPriceStartingAt("niedostępny jako nowy").build();
		assertEquals(expectedModel, actualModel);
	}
}
