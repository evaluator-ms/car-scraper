package com.glaud.carscraper.service;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.util.ReflectionTestUtils;

import com.glaud.carscraper.model.FuelQuantity;
import com.glaud.carscraper.model.FuelQuantity.Status;
import com.glaud.carscraper.model.RequestedCar;

@ExtendWith(SpringExtension.class)
public class FuelServiceImplIT {

	private FuelService fuelServiceImpl;
	private RequestedCar requestedCar;
	private FuelQuantity fuelQuantity;

	@BeforeEach
	public void setUp() {
		fuelServiceImpl = new FuelServiceImpl(new DocumentService());
		ReflectionTestUtils.setField(fuelServiceImpl, "fuelUrl", "https://www.autocentrum.pl/spalanie/");
	}

	@Test
	public void findFuelConsumtionPbTest() {
		fuelQuantity = new FuelQuantity(7.2, 6.8, "pb", Status.OK);
		assertEquals(fuelQuantity,
				fuelServiceImpl.findFuelConsumtion("/audi/a3/8p/hatchback-3d/silnik-benzynowy-1.6-102km-2003-2010/"));
	}

	@Test
	public void findFuelConsumtionOnTest() {
		fuelQuantity = new FuelQuantity(5.7, 5.3, "on", Status.OK);
		assertEquals(fuelQuantity,
				fuelServiceImpl.findFuelConsumtion("/audi/a3/8p/hatchback-3d/silnik-diesla-1.9-tdi-105km-2003-2012/"));
	}

	@Test
	public void findFuelConsumtionNotFoundTest() {
		fuelQuantity = new FuelQuantity(Status.NOT_FOUND);
		assertEquals(fuelQuantity,
				fuelServiceImpl.findFuelConsumtion("/alfa-romeo/146/silnik-benzynowy-1.3-90km-1997-2000/"));
	}

	@Test
	public void findFuelConsumtionCarPbTest() {
		requestedCar = new RequestedCar.Builder("audi", "a3", "silnik-benzynowy-1.6-102km").generation("8p")
				.version("hatchback-3d").build();
		fuelQuantity = new FuelQuantity(7.2, 6.8, "pb", Status.OK);
		assertEquals(fuelQuantity, fuelServiceImpl.findFuelConsumtion(requestedCar));
	}

	@Test
	public void findFuelConsumtionCarOnTest() {
		requestedCar = new RequestedCar.Builder("audi", "a3", "hatchback-3d/silnik-diesla-1.9-tdi-105km-2003-2012")
				.generation("8p").version("hatchback-3d").build();
		fuelQuantity = new FuelQuantity(5.7, 5.3, "on", Status.OK);
		assertEquals(fuelQuantity, fuelServiceImpl.findFuelConsumtion(requestedCar));
	}

}
