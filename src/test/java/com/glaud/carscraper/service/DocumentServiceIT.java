package com.glaud.carscraper.service;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.IOException;

import org.jsoup.nodes.Document;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.junit.jupiter.SpringExtension;


@ExtendWith(SpringExtension.class)
public class DocumentServiceIT {

	private DocumentService documentService;

	@BeforeEach
	public void setUp() {
		documentService = new DocumentService();
	}

	@Test
	public void getDocumentTest() throws IOException {
		Document doc = documentService.getDocument("https://www.autocentrum.pl/", "audi/");
		assertEquals("Audi - wszystkie modele, dane, silniki, testy • AutoCentrum.pl", doc.title());
	}
}
